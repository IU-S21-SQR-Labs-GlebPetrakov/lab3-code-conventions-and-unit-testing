package com.hw.db.controllers;

import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockStatic;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.Message;
import com.hw.db.models.Post;
import com.hw.db.models.Thread;
import com.hw.db.models.User;
import com.hw.db.models.Vote;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;


class ThreadControllerTest {

    private static final threadController THREAD_CONTROLLER = new threadController();
    private static final com.hw.db.models.Thread THREAD = thread();
    private static final MockedStatic<ThreadDAO> THREAD_DAO = mockStatic(ThreadDAO.class);
    private static final MockedStatic<UserDAO> USER_DAO = mockStatic(UserDAO.class);

    private static User user() {
        User user = new User();

        user.setNickname("mynameisjeff");
        user.setEmail("jeff@jeff.jeff");
        user.setFullname("Jeff");
        user.setAbout("My name is Jeff");

        return user;
    }

    private static Thread thread() {
        Thread thread = new Thread();

        thread.setId(42);
        thread.setForum("swamp");
        thread.setVotes(1337);
        thread.setSlug("yeet");
        thread.setMessage("Shrek");
        thread.setTitle("Shrek is god");

        return thread;
    }

    private static Post post() {
        Post post = new Post();

        post.setAuthor("jeff");
        post.setThread(42);
        post.setForum("swamp");
        post.setMessage("Shrek is definitely a god");

        return post;
    }

    @BeforeEach
    void setUp() {
        THREAD_DAO.reset();

        THREAD_DAO
                .when(() -> ThreadDAO.getThreadById(anyInt()))
                .thenReturn(THREAD);
        THREAD_DAO
                .when(() -> ThreadDAO.getThreadBySlug(anyString()))
                .thenReturn(THREAD);
        THREAD_DAO
                .when(() -> ThreadDAO.change(any(), anyInt()))
                .thenReturn(7);
    }

    @Test
    void getThreadBySlugTest() {
        String slug = "yeet";
        assertEquals(THREAD, THREAD_CONTROLLER.CheckIdOrSlug(slug));
    }

    @Test
    void getThreadInfoTest() {
        ResponseEntity responseEntity = THREAD_CONTROLLER.info("slug");

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(THREAD, responseEntity.getBody());
    }

    @Test
    void createPostTest() {
        USER_DAO.when(() -> UserDAO.Info(post().getAuthor())).thenReturn(user());

        ResponseEntity response = THREAD_CONTROLLER.createPost("hellothere", Collections.singletonList(post()));
        List<Post> posts = ((List<Post>) Objects.requireNonNull(response.getBody()));

        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(user().getNickname(), posts.get(0).getAuthor());
        assertEquals(THREAD.getId(), posts.get(0).getThread());
        assertEquals(THREAD.getForum(), posts.get(0).getForum());

    }

    @Test
    void getPostTest() {
        THREAD_DAO.when(() -> ThreadDAO.getPosts(
                anyInt(),
                anyInt(),
                anyInt(),
                anyString(),
                anyBoolean()
        )).thenReturn(singletonList(post()));

        ResponseEntity response = THREAD_CONTROLLER.Posts("hellothere", 1, 1, "flat", false);
        List<Post> posts = ((List<Post>) Objects.requireNonNull(response.getBody()));

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(post().getId(), posts.get(0).getId());
    }

    @Test
    void changeSlugTest() {
        String slug = "yeet";

        THREAD_DAO
                .when(() -> ThreadDAO.getThreadBySlug(slug))
                .thenThrow(EmptyResultDataAccessException.class);

        ResponseEntity response = THREAD_CONTROLLER.change(slug, THREAD);
        Message message = (Message) response.getBody();

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertEquals(
                new Message("Раздел не найден.").getMessage(),
                message.getMessage()
        );
    }

    @Test
    void createVote() {
        String slug = "yeet";
        Vote vote = mock(Vote.class);

        THREAD_DAO
                .when(() -> ThreadDAO.getThreadBySlug(slug))
                .thenThrow(EmptyResultDataAccessException.class);

        ResponseEntity response = THREAD_CONTROLLER.createVote(slug, vote);
        Message message = (Message) response.getBody();

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertEquals(new Message("Раздел не найден.").getMessage(), message.getMessage());
    }

}
